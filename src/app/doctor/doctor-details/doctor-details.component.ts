import { Component, OnInit, TemplateRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { DoctorService } from '../doctor.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-doctor-details',
  templateUrl: './doctor-details.component.html',
  styleUrls: ['./doctor-details.component.scss']
})
export class DoctorDetailsComponent implements OnInit {

  appointments: any = [];
  appointmentDates: string[] = [];
  selectedDate: any;
  selectedSlot: any;
  selectedDocId : any;
  selectedTimeSlots: any[] = [];
  slotIsAlreadyBooked: boolean = false;
  reviews : any = [];
  selectedDrId : any;
  DocReview : any =[]
  name: string = '';
  rating: number = 5;
  review: string = '';
  modalRef?: BsModalRef;
  appointmentBookForm!: FormGroup;
  docDetail: any = [];
  selectedDoctor: any = [];
  prescriptionBase64Path: any ='';

  constructor(public doctorService: DoctorService, public route: ActivatedRoute, private modalService: BsModalService, public fb: FormBuilder) {}

  ngOnInit(): void {
    this.appointmentBookForm = this.fb.group({
      p_fname: ['', Validators.required],
      p_lname: ['', Validators.required],
      gender: ['', Validators.required],
      contact: ['', [ Validators.required]],
      disease_name: ['',[ Validators.required]],
      symptom1: [''],
      symptom2: [''],
      id: [''],
      dp_id: [''],
      prescription: ['']
    });
    this.route.params.subscribe(param => this.selectedDocId = param['id']);
    this.getAppointmentsByDocId();
    this.getReviewsById();

  }

  getAppointmentsByDocId() {
    this.doctorService.getAppointmentsByDocId(this.selectedDocId).subscribe((data: any) => {
      data.map((item: any) =>{
        item.start_time = item.start_time.split(' ')[1],
        item.end_time = item.end_time.split(' ')[1]
      })
      this.appointments = data;
      this.appointmentDates = this.getUniqueAppointmentDates(this.appointments);
      this.selectedDate = this.appointmentDates[0];
      this.selectDate(null);
    })
  }

  private getUniqueAppointmentDates(appointments: any[]): string[] {
    const appointmentDates = appointments.map((appointment: any) => {
        return appointment.date;
      })
    return appointmentDates
       .filter((date: string, index: number) => appointmentDates.indexOf(date) === index);
  }

  selectDate = (event: any) => {
    const filteredSlots = this.appointments.filter((appointment: any) => appointment.date === this.selectedDate);
    this.selectedSlot = filteredSlots[0];

    this.selectedTimeSlots = filteredSlots;
  };

  getReviewsById(){
    this.doctorService.getReviewsById(this.selectedDocId).subscribe((data: any) =>{
      this.reviews = data;
      this.getDoctorDetail();
    })

  }

  postReviewsById(){
    const postData = {
      "review": this.review,
      "rating": this.rating,
      "dr_id": this.selectedDocId,
      "name": this.name
    }
    this.doctorService.postReviewsById(postData).subscribe((data: any) =>{
      Swal.fire({
        icon: 'success',
        title: 'Thank You',
        text: 'Your Review Added Successfully!',
      });
      this.getReviewsById();
      this.name="";
      this.rating =5;
      this.review = "";
    })

  }
  onRate($event:{oldValue:number, newValue:number}) {
    this.rating = $event.newValue;

  }

  onSubmit(template: any): void {
    if (this.selectedSlot.status === 'booked') {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Slot is already booked. Please choose available slot!',
      });
      return;
    } else{
      this.openModal(template);
    }
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  onBookAppointment() {
    const postData = {
      ...this.appointmentBookForm.value,
      dp_id: +this.selectedDocId,
      prescription:this.prescriptionBase64Path
    };
    console.log(postData);
    this.doctorService.addPatient(postData).subscribe(data => {
      if(data) {
        const aptData = {
          ...this.selectedSlot,
          ap_id: data['id'],
          status: 'booked',
          end_time: `${this.selectedSlot['date']} ${this.selectedSlot['end_time']}`,
          start_time: `${this.selectedSlot['date']} ${this.selectedSlot['start_time']}`
        }
        this.doctorService.bookAppointment(this.selectedDocId,aptData).subscribe(res =>{
          Swal.fire({
            icon: 'success',
            title: 'Good job!',
            text: 'Your appointment booked successfully!',
          });
          this.modalRef?.hide();
          this.getAppointmentsByDocId();
        })
      }
    },
    err => {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong',
      });
    })
  }

  getDoctorDetail() {
    this.doctorService.getAllDoctors().subscribe((data: any) => {
      this.selectedDoctor = data.filter((detail: any) => detail.id === +this.selectedDocId);

      if(this.reviews.length > 0){
        const result = this.reviews.reduce((accumulator: any, obj: any) => {
          return accumulator + obj.rating;
        }, 0);
        this.selectedDoctor[0]['ratings']=result/this.reviews.length;
      }
    })
  }



onChange(event: any) {
    if (event.target.value) {
      const file = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
          this.prescriptionBase64Path = reader.result;
      };
    } else alert('Nothing')
}
}
