import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DoctorService } from '../doctor.service';


@Component({
  selector: 'app-doctor-list',
  templateUrl: './doctor-list.component.html',
  styleUrls: ['./doctor-list.component.scss']
})
export class DoctorListComponent implements OnInit {
  doctorsList: any = []
  constructor(public router: Router, public doctorService: DoctorService) {

  }

  ngOnInit() {
    this.getAllDoctors();
  }

  getAllDoctors() {
    this.doctorService.getAllDoctors().subscribe(data => {
      console.log(data);
      this.doctorsList = data;
    })
  }

  goToDoctorDetails(docId: any) {
    this.router.navigate([`/doctors/${docId}`]);
  }

}
