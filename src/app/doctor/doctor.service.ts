import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DoctorService {

  constructor(private http: HttpClient) { }

  getAppointmentsByDocId(docId: any) {
    return this.http.get(`${environment.apiUrl}doctors/${docId}/appointments`);
  }

  getAllDoctors() {
    return this.http.get<any>(`${environment.apiUrl}doctors`);
  }

  getReviewsById(drId: any) {
    return this.http.get<any>(`${environment.apiUrl}doctors/${drId}/reviews`);
  }

  postReviewsById(data:any) {
    return this.http.post<any>(`${environment.apiUrl}add-review`,data);
  }
  addPatient( body: any) {
    return this.http.post<any>(`${environment.apiUrl}add-patient`, body).pipe(catchError(err =>  err));
  }
  bookAppointment(docId: any, body: any) {
    return this.http.put<any>(`${environment.apiUrl}doctors/${docId}/book-appointment`, body);
  }
}
